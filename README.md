## Version 1.35.1 (1.35.1)

Install all this extension first

1. Apache Conf by mrmlnc
2. Bootstrap 4, Font Awesome 4, Font Awesome 5 Free & Pro snippets by Ashok Koyi
3. DotENV by mikestead
4. ftp-sync by Lukasz Wronski
5. Go by Microsoft
6. Highlight Matching Tag by vincaslt
7. Material Icon Theme by Philipp Kief
8. PHP Intelephense by Ben Mewburn
9. PHP IntelliSense by Felix Becker
10. Settings Sync by Shan Khan
11. Spacegray VSCode by Mihai Vilcu


How to use settings.json file.

1. Download VS Code from (https://code.visualstudio.com/).
2. open settings press [CMD + ,] on MacOS or [Ctrl+,] on Windows then click curly bracket '{}' from top right corner.
3. paste all text from settings.json to vscode settings.
